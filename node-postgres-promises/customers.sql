DROP DATABASE IF EXISTS customers;
CREATE DATABASE customers;

\c customers;

CREATE TABLE customer (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  age INTEGER,
  sex VARCHAR
);

INSERT INTO customer (name, age, sex)
  VALUES ('Tyler',  30, 'M');