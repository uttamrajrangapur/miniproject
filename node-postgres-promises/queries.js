var promise = require('bluebird');
var moment = require('moment');
var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://ecom-uttamraj.kr@localhost:5432/customers';
var db = pgp(connectionString);

// add query functions
  async function getAllCustomers(req, res, next) {
    try{
      let response = await db.any('select * from customer');
      res.status(200)
            .json({
              status: 'success',
              data: response,
              message: 'Retrieved ALL customers'
            });
    }
    catch(err){
      return next(err);
    }
}

async function getSingleCustomer(req, res, next) {
  try{
    var customerID = parseInt(req.params.id);
    let response = db.one('select * from customer where id = $1', customerID);
        res.status(200)
          .json({
            status: 'success',
            data: response,
            message: 'Retrieved SPECIFIED CUSTOMER'
          });
    
  }
  catch(err) {
        return next(err);
      }
  }

  async function createCustomer(req, res, next) {
    try{
      req.body.age = parseInt(req.body.age);
      await db.none('insert into customer(name, age, sex)' +
        'values(${name}, ${age}, ${sex})',
      req.body);
      res.status(200)
          .json({
            status: 'success',
            message: 'Inserted one customer'
          });
      
    
    }
    catch(err) {
        return next(err);
    }
  }

  async function updateCustomer(req, res, next) {
    try{
      await db.none('update customer set name=$1,age=$2, sex=$3 where id=$4',
      [req.body.name, parseInt(req.body.age),
        req.body.sex, parseInt(req.params.id)]);
      res.status(200)
      .json({
        status: 'success',
        message: 'Updated customer'
      });
    
    }
    catch(err) {
        return next(err);
      }
  }

  async function removeCustomer(req, res, next) {
    try{
      var CustomerID = parseInt(req.params.id);
      let response = await db.result('delete from customer where id = $1', CustomerID);
        
          /* jshint ignore:start */
          res.status(200)
            .json({
              status: 'success',
              message: 'Removed ${result.rowCount} customer'
            });
          /* jshint ignore:end */
    }
    catch(err) {
        return next(err);
      }
  }

  function compareDates(req,res,next){
    try{
      var dateA=moment(req.body.dateA);
      var dateB=moment(req.body.dateB);
      var result = "";
      if(moment(dateA).isBefore(dateB)){
        result+="dateA is earlier";
      }else if(moment(dateB).isBefore(dateA)){
        result+="dateB is earlier";
      }else if(moment(dateA).isSame(dateB)){
        result+="both dates are same";
      }else{
        result+="error in dates";
      }
      res.status(200).json({
        status:'success',
        data:result,
        message:"date function successfully executed"
      });

    }
    catch(err){
      return next(err);
    }
  }

module.exports = {
  getAllCustomers: getAllCustomers,
  getSingleCustomer: getSingleCustomer,
  createCustomer: createCustomer,
  updateCustomer: updateCustomer,
  removeCustomer: removeCustomer,
  compareDates:compareDates
};