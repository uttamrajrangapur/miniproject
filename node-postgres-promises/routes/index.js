var express = require('express');
var router = express.Router();

var db = require('../queries');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/customers', db.getAllCustomers);
router.get('/api/customers/:id', db.getSingleCustomer);
router.post('/api/customers', db.createCustomer);
router.put('/api/customers/:id', db.updateCustomer);
router.delete('/api/customers/:id', db.removeCustomer);
router.post('/api/compareDates',db.compareDates);
module.exports = router;
